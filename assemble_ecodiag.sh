#!/bin/bash

ECODIAG_PATH=public/ecodiag

ECODIAG_JS_SRC="utils.js parameters.js devices.js interface_helpers.js device_module.js cluster_module.js ecodiag.js plot_helpers.js minify.json.js"

ECODIAG_OTHER_SRC=$ECODIAG_JS_SRC" co2_style.css unlock_icon.png d3.v4.min.js lock_icon.png"

mkdir -p $ECODIAG_PATH

rm -rf $ECODIAG_PATH/*

mkdir -p $ECODIAG_PATH/data

for f in $ECODIAG_OTHER_SRC ; do
  cp public/$f $ECODIAG_PATH/
done

cp public/ecodiag.html $ECODIAG_PATH/index.html
cp public/data/ecodiag_exemple.json $ECODIAG_PATH/data/
cp LICENSE $ECODIAG_PATH/

tar czf public/ecodiag.tgz $ECODIAG_PATH

# cd public
# cat $ECODIAG_JS_SRC > ../tmp_full_ecodiag.js
# cd -
# python3 -m jsmin tmp_full_ecodiag.js > $ECODIAG_PATH/full_ecodiag.js
# rm tmp_full_ecodiag.js

# # for f in $ECODIAG_JS_SRC ; do
#   # python3 -m jsmin public/$f > $ECODIAG_PATH/$f
# # done

# cat public/ecodiag.html | tr '\n' ' ' | sed -e 's/utils\.js.*plot_helpers\.js/full_ecodiag.js/g' > $ECODIAG_PATH/index.html