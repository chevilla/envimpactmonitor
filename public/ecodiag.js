

g_data['user1'] = {

  devices: { // nb
    'desktop/ecodiag_avg_PC':   0,
    'desktop/avg_WS':   0,
    'laptop/ecodiag_avg_laptop':    0,
    'screen':                       0,
  },

  clusters: {
    jeanzay_cpu: {hours_nodes: 0}
  },

  working_days: 210

};

var conv = { to_CO2: {elec: 0.084 } };

function convfactor(name,type) {
  if(type!="CO2")
    return 0;
  else if(name.includes('elec'))
    return conv.to_CO2.elec
  else if(name.includes('CO2'))
    return 1;
  else
    console.log("WARNING: " + name + " ; " + type)
  
  return undefined;
}

g_data['user']= clone_obj(g_data['user1']);
g_data['user2']= clone_obj(g_data['user1']);

var equiv_data = {
  // https://jancovici.com/transition-energetique/charbon/a-quoi-sert-le-charbon/
  // charbon bitumineux : 8 MWh de chaleur par tonne ; environ 3 MWh d’électricité par tonne
  "charbon": {factor: 1./3., unit:" kg de "},
  // 4 MWh de chaleur par tonne ; environ 1,3 MWh d’électricité par tonne
  // "lignite": {factor: 1./(1.3), unit:" kg de "},

  // 160Wc/m2, ~1000kWh/kWc, 94.4% conv DC/AC, 90% batterie
  "panneau photovoltaïque": {factor: 1./(1000*0.160*0.944*0.9), unit:" m<sup>2</sup> de "},
}

function init_ecodiag_interface() {

  params.viz.user_detailed_barplot = true;

  document.getElementById('ecodiag_ph').innerHTML = `

<!--<label>Vue détaillée : <input type="checkbox" id="input_detailed_barplot" checked="True"></label> <br>-->

<div class="tab">
  <button class="tablinks tabactive" onclick="open_tab(event, 'devices_tab', 'tabcontent2')">Équipement</button>
  <!--<button class="tablinks " onclick="open_tab(event, 'clusters_ph', 'tabcontent2')">Clusters</button>-->
  <button class="tablinks " onclick="open_tab(event, 'data_tab', 'tabcontent2')">Données</button>
</div>

<div id="devices_tab" class="tabcontent2" style="display:block;">
<div id="devices_ph"></div>

<p class="whatif">
  Total CO2 annuel : <span class="value" id="ph_device_total"></span> kg.CO2e/an
    = <span class="value" id="ph_device_total_grey"></span> (fabrication et transport)
    + <span class="value" id="ph_device_total_elec"></span> (consommation électrique).
</p>

<p class="whatif">
  Bilan consommation électrique : <span class="value" id="ph_total_kWh"></span> kWh/an,
  soit l&#39;équivalent de la production de <span class="value" id="ph_total_kWh_equiv"></span> <span id="ph_total_kWh_equiv_unit"></span>
  <select id="ph_total_kWh_equiv_select"></select>.
</p>

<p class="whatif">
  Économies réalisables par l&#39;augmentation de la durée de vie de mes équipements : <span class="value" id="ph_lifetime_gain"></span> kg.CO2 par an.
</p>






</div>

<!--<div id="clusters_ph" class="tabcontent2">
</div>-->

<div id="data_tab" class="tabcontent2">

<p></p>

<table>
    <tr><td>Importer une liste d&#39;équipements :</td>
        <td><input type="file" id="import_json_file" /</td>
        <td><span class="note">(voir <a href="data/ecodiag_exemple.json">exemple.json</a>)</span></td></tr>
    <tr><td>Exporter la liste d&#39;équipements :</td>
        <td>
          <form id="export_form">
            <input type="text" class="filename" id="export_filename" value="my_device_list"/><span class="unit">.json</span>
            <input id="export_submit" type="submit" value="Save"/>
          </form>
        </td>
        <td></td></tr>
    <tr></tr>
    <tr><td>Coût carbone d&#39;1kWh électrique :</td>
        <td><input type="number" id="input_kWh2CO2" min="0.04" max="2" step="0.002"> <span class="unit">kg eqCO<sub>2</sub></span></td>
        <td class="note"></td></tr>

    <tr><td>Télécharger le fichier de données :</td>
        <td><a href="devices.js" target="_blank">devices.js</a></td>
        <td class="note"></td></tr>

  </table>
<p>
</p>
</div>

`;

  var ecodiag_ui = {};

  ecodiag_ui.update = function() {

    g_user_data1 = assemble_user_data(g_year);
    g_user_data2 = assemble_user_data(g_year,2);

    var devices1 = process_raw_device_list(g_data['user1'].devices, {type_renamer: t => 'total'}, g_data['user1'].working_days);
    var devices2 = process_raw_device_list(g_data['user2'].devices, {type_renamer: t => 'total'}, g_data['user2'].working_days);

    var gain_grey_CO2 = devices1.grey.total_CO2 - devices2.grey.total_CO2;
    if(Number.isNaN(gain_grey_CO2))
      gain_grey_CO2 = 0;
    document.getElementById('ph_lifetime_gain').textContent = toFixed(gain_grey_CO2,0);

    barplot.update(g_year,g_value_type);
  }

  function assemble_user_data(selected_year,suffix) {
    user_name = "user";
    if(suffix)
      user_name += suffix;
    else
      user_name += "1";
    user_param = g_data[user_name];

    // console.log(user_param.devices);
    var devices = process_raw_device_list(user_param.devices,
      {type_renamer: function(t) {
          var res = {
            'desktop':'desktop',
            'screen':'screen',
            'laptop':'laptop',
          }[t];
          if(!res)
            res = 'other';
          return res;
        }
      }
      ,null);

    var sorted_devices = {grey:{},elec:{}};
    ['desktop','screen','laptop','other'].forEach(function(k){
      if(devices.grey[k+"_CO2"])
        sorted_devices.grey["ecodiag_"+k+"_CO2"]   = devices.grey[k+"_CO2"];
      if(devices.elec[k+"_elec"])
        sorted_devices.elec["ecodiag_"+k+"_elec"]  = devices.elec[k+"_elec"];
    });

    var elec_device  =  keyval_sumall(devices.elec);

    var res = {
      
      ecodiag_fabric_and_transport: sorted_devices.grey,
      ecodiag_elec: sorted_devices.elec,
      // clusters: process_cluster_list(user_param.clusters),
    };

    if(suffix!=2) {
      document.getElementById('ph_device_total').textContent = toFixed(keyval_sumall(devices.grey) + elec_device * conv.to_CO2.elec, 1);
      document.getElementById('ph_device_total_grey').textContent = toFixed(keyval_sumall(devices.grey));
      document.getElementById('ph_device_total_elec').textContent = toFixed(elec_device * conv.to_CO2.elec, 1);

      document.getElementById('ph_total_kWh').textContent = toFixed(elec_device, 1);
      var eqd = equiv_data[document.getElementById("ph_total_kWh_equiv_select").value];
      if(eqd) {
        document.getElementById('ph_total_kWh_equiv_unit').innerHTML = eqd.unit;
        document.getElementById('ph_total_kWh_equiv').textContent = toFixed(elec_device * eqd.factor, 1); 
      }

    }

    return res;
  }

  var g_user_data1 = assemble_user_data(g_year,1);
  var g_user_data2 = assemble_user_data(g_year,2);

  function make_user_data_for_barplot(selected_year, value_type, opt) {

    var user_data1_cpy = clone_obj(g_user_data1);
    var user_data2_cpy = clone_obj(g_user_data2);

    // currently there is no "objective" for electricity consumption nore cluster usage, so:
    delete user_data2_cpy.ecodiag_elec;
    delete user_data2_cpy.clusters;

    if(opt.stacked=='full') {

      var full_data1 = compute_CO2_and_kWh(user_data1_cpy);
      var full_data2 = compute_CO2_and_kWh(user_data2_cpy);

      // stack data
      var subgroups         = ['ecodiag_fabric_and_transport','ecodiag_elec','clusters'];
      subgroups.reduce_list = ['ecodiag_fabric_and_transport','ecodiag_elec','clusters'];
      var yReduced1 = ungoup(full_data1, subgroups);
      var r1 = process_group(yReduced1,'actuel')
      var r2 = process_group(ungoup(full_data2, subgroups),'objectif');
      r2.dim = 0.5;//params.viz.mosaic_barplot ? 1 : 0.5;
      var res = [r1,r2];
      return res;
    } else {
      var datatmp = compute_CO2_and_kWh(user_data1_cpy, {}, user_data2_cpy);
      if(!(params.viz.user_detailed_barplot || params.viz.mosaic_barplot)) {
        var reduce_list = ['ecodiag_fabric_and_transport','ecodiag_elec','clusters'];
        return partial_reduce_inplace(datatmp, reduce_list);
      }
      return datatmp;
    }
  }

  var barplot = make_yearly_barplot([g_user_data1,g_user_data2], g_year, g_value_type, 'user',
                            make_user_data_for_barplot,
                            {dir:'horizontal',width: 700, height:150});


  // checkbox to split/unsplit the user-report bar plot
  // document.getElementById('input_detailed_barplot').addEventListener("change", function() {
  //     params.viz.user_detailed_barplot = this.checked;
  //     ecodiag_ui.update();
  // } );


  var device_ui = init_devices('devices_ph', g_data.user1, g_data.user2, ecodiag_ui.update,
    {
      show_usage:false,
      default_map: {
        'desktop': 'ecodiag_avg_PC',
        'laptop':      'ecodiag_avg_laptop',
        'printer':     'office_40_99kg',
      }});
  //init_clusters('clusters_ph', g_data.user1, ecodiag_ui.update);

  init_num_controller('input_kWh2CO2',conv.to_CO2,'elec', null, function() { device_ui.sync(); ecodiag_ui.update(); } );

  {
    var equiv_select = document.getElementById("ph_total_kWh_equiv_select");
    
    for(var item in equiv_data) {
      var opt_el = document.createElement('option');
      opt_el.value = item;
      opt_el.innerHTML = tr(item);
      equiv_select.appendChild(opt_el);
    }

    equiv_select.addEventListener("change",ecodiag_ui.update);
  }

  function handle_ecodiag_json_file_select(files) {
    if(files.length==1){
      var file = files[0];
      var reader = new FileReader();

      reader.onload = (function(theFile) {
          return function(e) {
            var json_text = e.target.result;
            // remove first line if needed:
            if(json_text.startsWith("var"))
              json_text = json_text.split("\n").slice(1).join("\n");
            // remove comments:
            json_text = JSON.minify(json_text);
            var imported_data = JSON.parse(json_text);

            if(imported_data.kWh2CO2)
              conv.to_CO2.elec = imported_data.kWh2CO2;
            
            g_data['user1'] = imported_data;
            g_data['user']  = clone_obj(g_data['user1']);
            g_data['user2'] = clone_obj(g_data['user1']);
            device_ui.sync(g_data['user1'],g_data['user2']);
            ecodiag_ui.update();

          };
        })(file);

      reader.readAsText(file);
    }
  }

  document.getElementById('import_json_file').addEventListener('change', function(evt) { return handle_ecodiag_json_file_select(evt.target.files); }, false);

  document.getElementById('export_form').onsubmit = function(e) {
    e.preventDefault();

    var blob = new Blob([JSON.stringify(g_data['user1'], null, 2)], {
      "type": "application/json"
    });
    var a = document.createElement("a");
    var filename = document.getElementById('export_filename').value
    
    a.download = filename;
    a.href = URL.createObjectURL(blob);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  ecodiag_ui.update();

  return ecodiag_ui;
}

