
g_data['perso'] = {

  travels: { // km
    bike:          0,
    ebike:         0,
    motorbike:     0,
    bus:         200,
    tgv:        3000,
    flight_loc:  580, // according to https://www.ecologique-solidaire.gouv.fr/sites/default/files/bulletin_stat_trafic_aerien_2018.pdf,
    flight:     1200, // half of the journeys can be considered "local", but their distance is of course much shorter...
                      // the sum match the target of 500 kgCO2e (source: voir default_french_citizen)

    car:        7400, // match avg french 2000 kgCO2e, (source: voir default_french_citizen)
  },

  // TODO + fret & distribution 383 kg.CO2
  
  // meals imported from "user", TODO + boissons, ptit dej, etc.

  home_energy: {
    elec:        1554,  // kWh, match avg french 171 kgCO2e, (source: voir default_french_citizen)
    gaz_nat_m3:   330,  // m^3, match avg french 654 kgCO2e, (source: voir default_french_citizen)
    gaz_pp_kg:    158,  // kg,  match avg french 500 kgCO2e, (source: voir default_french_citizen)
    wood_kg:      300,  // source: 7.6 MTep en france / 66M de français (-> 787 kgCO2e !!)
                        // 1 Tep = 11 630 kWh 
                        // 1 Tep = 6.8 steres
                        // - en 2016 8M de foyers équipés [http://www.enr.fr/editorial/173/Le-chauffage-au-bois-domestique]
                        // - [https://www.total-proxi-energies.fr/particuliers/actualites/le-bois-de-chauffage-enjeu-strategique-pour-la-transition-energetique-francaise]
                        //    - 70% de la conso de bois-énergie pour le chauffage domestique 
                        //    - 6,8 M de personnes utilisent un appareil de chauffage au bois en France (48% l’utilisent comme chauffage principal)
                        //    - près de 90% du combustible utilisé dans ces appareils est du bois bûche
                        //  -> (7.6 MTep * 70%) / 7M -> 8000 kWh ou 5 steres (-> 550 kgCO2e !!)
                        // - étude détaillé: https://www.ademe.fr/sites/default/files/assets/documents/90037_rapport-etude-chauffage-domestique-bois.pdf

    // TODO + eau dechet 140 kgCO2e
    // TODO + specifique: 171 kgCO2e
    // TODO + chaleur reseau: 62 kgCO2e
  },
  home_equipment: { // -> energie grise, 157kg.CO2
    lave_linge:      1, 
    lave_vaisselle:  1,
    seche_linge:     1
  },

  house: {
    type: 'house', // or 'flat'
    surface: 90, // m^2; -> module energie grise construction + mobilier
    adults: 2,
    teens: 1,
    kids:  1
  },

  secondary_house: {
    type: 'none',
    surface: 0, // -> energie grise
  },

  devices: { // nb, -> energie grise + estimation
    desktop:        0, 
    screen:         0,
    laptop:         1,
    pad:            1,
    smartphone:     1,
    printer:        1,
    console:        1,
    TV:             2,
    box:            1
  },

  things: {
    clothes: 10, // nb pieces a year

  },
};

g_data['perso2'] = clone_obj(g_data['perso']);

