
var inria_nat = {
  // '2012': {
  //   travels: { // km
  //     flight:     28.9*1000*1000,
  //     flight_loc:  3.4*1000*1000, // France only
  //     train:      (44.3-3.4-28.9)*1000*1000,
  //   }
  // },
  '2015': {
    travels: { // km
      flight:     31076162,
      flight_loc:  2749860, // France only
      train:       5691461 //  (41.3-2.8-27.6)*1000*1000,
    }
  },
  '2016': {
    travels: { // km
      flight:     29011897,
      flight_loc:  2788923, // France only
      train:       5817304
    }
  },

  '2017': {
    travels: { // km
      flight:     30001736,
      flight_loc:  2868111, // France only
      train:       5725728,
    }
  },

  '2018': {
    travels: { // km
      flight:     26343330,
      flight_loc:  2667967, // France only
      train:      5666274,
    }
  },

  '2019': {
    travels: { // km
      flight:     26998775,
      flight_loc:  2079864, // France only
      train:       6115812,
    }
  },
};
