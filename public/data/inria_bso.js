
var center_default = {
  cafeteria : {
    beef: 32,   chicken: 32,   fish: 16, vege: 15, vegan: 1 // average per day, TODO: au pif!
  },

  elec: {
    '2015': 1392451, // Source: JPB
    '2016': 1565568,
    '2017': 1543207,
    '2018': 1557631,
    '2019': 1306348*(1 + 1/11*1.2), // mesure sur 11 mois (il manque décembre, x1.2 car conso + importante que la moyenne), == 1448858
  },
  
  to_office: { // average km per worker, per way
    
    // version au pif : 
    // bike:   0.09*5,  // 9%,  5km
    // ebike:  0.03*7,  // 3%,  7km
    // bus:    0.12*8,  // 12%, 8km
    // train:  0.02*30, // 2%,  30km
    // car:    0.74*15, // 74%, 15km

    // version basée sur le sondage mobilité 2018 + un peu de pif pour extrapoler
    //  = km moyen * poucentage
    walk:   1.5 * 0.09,
    bike:   5   * 0.1,
    ebike:  7   * 0.09,
    bus:    5.3 * 0.06,
    tram:   4.7 * 0.16,
    ter:    55  * 0.025,
    car:    12.5* 0.49
  },

  bought_devices: { // nombre d'unités achetées sur l'année
    desktop:40,     // au pif
    laptop:40,      // au pif
    screen:40,      // au pif
  },

  devices: { // nombre d'unités utilisées sur l'année
    desktop:150,      // au pif
    laptop:100,      // au pif
    screen:400,      // au pif
  },

  workers: 260,           // -> stats challenge mobilité 2018 = 260
  researchers: 140,       // au pif
  support_staff: 120,     // au pif
  travelers: 200,         // au pif
  building_workers: 200,  // au pif
  building_seats: 9*(7*2+12) + (3*2+2*6+3) /*R5*/ + (6+6+3) /*R1*/ + (2+4) /*R2*/ + (2*7+3+6) /*R4*/, // = 299

  // pour 2018, total avion:  2 265 330 dont  271 135 pour les "short"
  // train:  513 835 
  // voiture/bus: 2600 trajets, 15km par trajet, 50% voiture 50% bus

      
  '2015': {
    travels: {
      flight:       2561900 /* international */ + 357220 /* EU */,
      flight_loc:    463051 /* domestic */,
      tgv:          403952,
    },
  },

  '2016': {
    travels: {
      flight:       1962531 /* international */ + 439789 /* EU */,
      flight_loc:    474725 /* domestic */,
      tgv:          451903,
    },
  },

  '2017': {
    travels: {
      flight:       2217724 /* international */ + 376901 /* EU */,
      flight_loc:    397573 /* domestic */,
      tgv:          539998,
    },
  },

  '2018': {
    travels: {
      flight:       1959228 /* international */ + 357978 /* EU */,
      flight_loc:    282787 /* domestic */,
      tgv:          571044,
    },
  },
        
  '2019': {
    travels: {
      flight:       1877591 /* international */ + 369912 /* EU */,
      flight_loc:    147955 /* domestic */,
      tgv:          553598,
    },
  },
};