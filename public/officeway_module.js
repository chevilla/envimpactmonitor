
function init_office_way(div_id,in_data1,in_data2,update_callback) {

  var g_office_ways = [];
  var data1;
  var data2;

  document.getElementById(div_id).innerHTML = `
    <div style="position:relative;" name="office_table_block">
      <input type="button" value="+" name="officeway_add_row" style="position: absolute;bottom: 2px;">
      <table name="officeway_table" style="margin-left:2em;">
      </table>
    </div>
  `;

  var officeway_table = document.querySelector('#'+div_id + ' table[name="officeway_table"]');

  var res = {};
  res.sync = function(new_data1,new_data2) {
    if(new_data1) {
      data1 = new_data1;

      // rebuild the interface from scratch
      officeway_table.innerHTML = `
        <tr><th colspan="2">Actuel</th><th class="transparent"></th><th colspan="2">Objectif</th><td class="transparent"></td></tr>
        <tr><th>aller-retour</th><th>moyen</th><th class="transparent"></th><th>aller-retour</th><th>moyen</th><td class="transparent"></td></tr>
        `;
      g_office_ways = [];
    }
    if(new_data2) {
      data2 = new_data2;
    }

    keyval_foreach(data1, function(k,v) { if(v>0) add_officeway_row(k,v) });

    sync_officeway();
  }
      


  function sync_officeway() {

    keyval_clear(data1);
    if(data2)
      keyval_clear(data2);

    g_office_ways.forEach(function (item) {

      data1 = keyval_append(data1,item.curway.value,Number(item.curkm.value));
      if(data2)
        data2 = keyval_append(data2,item.newway.value,Number(item.newkm.value));

      if(item.newkm.disabled && item.newkm.value != item.curkm.value) {
        item.newkm.value = item.curkm.value;
      }
    });
  }

  // Function to add a row in the officeway_table
  function add_officeway_row(item,dist)
  {
    var tr_el       = document.createElement('tr');
    var td_km       = document.createElement('td');
    var td_way      = document.createElement('td');
    var td_km_new   = document.createElement('td');
    var td_way_new  = document.createElement('td');

    var cur_officeway_el = officeway_selector_el.cloneNode('deep');
    cur_officeway_el.value = item;
    var new_officeway_el = officeway_selector_el.cloneNode('deep');
    if(item=='bike' || dist<=5*2)
      new_officeway_el.value = 'bike';
    else if(dist<=12*2)
      new_officeway_el.value = 'ebike';
    else
      new_officeway_el.value = 'ter';

    td_way.appendChild(cur_officeway_el);
    td_way_new.appendChild(new_officeway_el);

    var cur_km_el = document.createElement('input');
    cur_km_el.type='number';
    cur_km_el.step='1';
    cur_km_el.min='0';
    cur_km_el.max='500';
    cur_km_el.value = dist;
    td_km.appendChild(cur_km_el);
    var km_el = document.createElement('span');
    km_el.innerHTML = "km";
    km_el.className = 'unit';
    td_km.appendChild(km_el);

    var new_km_el = cur_km_el.cloneNode('deep');
    new_km_el.disabled = true;
    create_locker(new_km_el,td_km_new);
    td_km_new.appendChild(new_km_el);
    td_km_new.appendChild(km_el.cloneNode('deep'));

    cur_km_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    cur_officeway_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    new_km_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    new_officeway_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });
    
    // record the different inputs for future uses/deletion
    g_office_ways.push({
      curway: cur_officeway_el,
      curkm:  cur_km_el,
      newway: new_officeway_el,
      newkm:  new_km_el,
    });

    tr_el.appendChild(td_km);
    tr_el.appendChild(td_way);
    tr_el.appendChild(td_sep.cloneNode("deep"));
    tr_el.appendChild(td_km_new);
    tr_el.appendChild(td_way_new);
    tr_el.appendChild(td_sep.cloneNode("deep")); // TODO add a delete button within this cell
    officeway_table.appendChild(tr_el);
  }

  // Create a reference "office-way" selector object to be cloned on demand
  var officeway_selector_el = document.createElement('select');
  officeway_types.forEach(function(item) {
    var opt_el = document.createElement('option');
    opt_el.value = item;
    opt_el.innerHTML = tr(item);
    officeway_selector_el.appendChild(opt_el);
  });

  res.sync(in_data1,in_data2);

  document.querySelector('#'+div_id + ' input[name="officeway_add_row"]').addEventListener('click',function(){
    add_officeway_row('car',0);
    var content = document.getElementById(div_id).parentNode;
    content.style.maxHeight = content.scrollHeight + "px";
  });

  return res;
}
