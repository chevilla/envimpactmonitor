


function add_user_pro_travels_rows(table_id,update_callback) {
  // lien pertinent pour le chauffage gaz : http://www.carbone4.com/analyse-chaudieres-gaz-climat/
  add_rows(document.getElementById(table_id),g_data.user,g_data.user2,update_callback,
    [
      //{group:'travels',     id:'car',         min:0, max:10000, step:500, unit:'km', label:'Voiture :', note:''},
      {group:'travels',     id:'flight_loc',  min:0, max:99999, step:100, unit:'km', label:'Vols "locaux" :', note:'(destinations atteignables en train de manière raisonable)'},
      {group:'travels',     id:'flight',      min:0, max:99999, step:100, unit:'km', label:'Autres vols :', note:''},
      {group:'travels',     id:'tgv',         min:0, max:99999, step:100, unit:'km', label:'Train :', note:'(tgv)'},
      {group:'travels',     id:'bus',         min:0, max:99999, step:100, unit:'km', label:'Bus :', note:''},
    ]);
}


function init_user_interface(perso_interface) {

	document.getElementById('user_ph').innerHTML = `
	<!--
<form id="value_type_selector">
  <input type="radio" name="value_type_selector" value="CO2" checked="checked"><label>CO2</label><br>
  <input type="radio" name="value_type_selector" value="kWh"><label>kWh</label><br>
</form>
-->

<label>Detailed view: <input type="checkbox" id="user_detailed_barplot" ></label>
<span style="display:none"><label>Mosaic view: <input type="checkbox" id="mosaic_barplot" ></label></span>
<!--<label>Fuse pie-charts: <input type="checkbox" id="fuse_piechart_comp" ></label>-->


<div class="tab">
  <button class="tablinks tabactive" onclick="open_tab(event, 'user_gen', 'tabcontent2')">Généralités</button>
  <button class="tablinks " onclick="open_tab(event, 'user_travels', 'tabcontent2')">Missions</button>
  <button class="tablinks " onclick="open_tab(event, 'user_office_way', 'tabcontent2')">Domicile-travail</button>
  <button class="tablinks " onclick="open_tab(event, 'user_devices_ph', 'tabcontent2')">Équipement</button>
  <button class="tablinks " onclick="open_tab(event, 'user_meals', 'tabcontent2')">Alimentation</button>
  <button class="tablinks " onclick="open_tab(event, 'user_clusters_ph', 'tabcontent2')">Clusters</button>
  <button class="tablinks " onclick="open_tab(event, 'user_notes', 'tabcontent2')" style="float:right;">Notes</button>
</div>

<!-- <button class="collapsible">Généralités</button>
<div class="content"> -->
<div id="user_gen" class="tabcontent2" style="display:block;">
  <p></p>
  <table>
    <tr><td>Travail :</td>
        <td><input type="number" step="1" min="1" max="365" id="input_user_working_days"> <span class="unit">jours/ans</span></td>
        <td class="note">Ex. 5 j/sem. * 52.14 sem. - 35 (congés) - 8 (RTT) - 8 (fériés)</td></tr>
    <!--<tr><td>Internet :</td>
        <td><input type="number" step="0.5" min="0" max="9" id="input_user_internet"> <span class="unit">h/jours</span></td>
        <td class="note">TODO</td></tr>-->
    <tr><td>Batiment, conso élec :</td>
        <td><input type="number" step="1000" min="0" max="99999999" id="input_user_bat_elec"> <span class="unit">kWh</span></td>
        <td class="note">hors clusters et équipements individuels</td></tr>
    <tr><td>Batiment, conso gaz :</td>
        <td><input type="number" step="1000" min="0" max="99999999" id="input_user_bat_gaz"> <span class="unit">kWh</span></td>
        <td class="note"></td></tr>
    <tr><td>Batiment, réseau de chaleur :</td>
        <td><input type="number" step="1000" min="0" max="99999999" id="input_user_bat_heatnet"> <span class="unit">kWh</span></td>
        <td class="note">facteur : <input type="number" step="0.01" min="0.02" max="0.4" id="input_user_heatnet_factor"> <span class="unit">kgCO2e/kWh</span></td></tr>
    <tr><td>Batiment, taille :</td>
        <td><input type="number" step="1" min="1" max="9999" id="input_user_bat_workers"> <span class="unit">agents</span></td>
        <td class="note"></td></tr>
  </table>
  <p></p>
</div>



<!-- <button class="collapsible">Missions</button>
<div class="content"> -->
<div id="user_travels" class="tabcontent2">
  <p></p>
  <table id="user_mission_table">
    <tr><th class="transparent"></th><th class="transparent"></th><th>actuel</th><th class="transparent"></th><th>objectif</th><th class="transparent"></th><th class="transparent"></th></tr>
  </table>
  <p class="whatif">
    Si le train avait été utilisé à la place de l&#39;avion pour les vols locaux, <span class="value" id="ph_user_local_flight_gain"></span> kg.CO2e auraient ainsi été évité pour l&#39;année courante,
    soit <span class="value" id="ph_user_local_flight_COP21"></span> % de la cible COP21.
  </p>
</div>



<!-- <button class="collapsible">Trajets domicile-travail</button>
<div class="content"> -->
<div id="user_office_way" class="tabcontent2">
<p></p> <div id="user_office_way_ph"></div> 
<!-- <p>
  Distance : <input type="number" step="1" min="1" max="500" id="ph_office_distance" name="ph_office_distance" > km, moyen :
  <select id="current_office_way">
  </select>.
</p> -->

<p>TODO : ajouter paramétrage de la taille de la voiture (e.g., pot de yaourt essence VS suv élec)</p>

<p class="whatif">
  Mon future mode de transport permettrait d&#39;économiser <span class="value" id="ph_office_way_gain"></span> kg.CO2 par an,
  soit l&#39;équivalent de <span class="value" id="ph_office_way_COP21"></span> % de la cible COP21.

  <!-- Si je vais au travail en
  <select id="new_office_way">
  </select>,
  j&#39;économiserais <span class="value" id="ph_office_way_gain"></span> kg.CO2 par an,
  soit l&#39;équivalent de <span class="value" id="ph_office_way_COP21"></span> % de la cible COP21. -->

  <!--

I&#39;m living at <span id="ph_office_distance"></span> km of my office,
if instead of going to work by <span id="ph_office_way"></span>, I go by
<select id="new_office_way">
  <option value="ebike">ebike</option>
  <option value="bike">bike</option>
  <option value="bus">bus</option>
  <option value="train">train</option>
  <option value="car">car</option>
</select>,
I&#39;ll save <span id="ph_office_way_gain"></span> kg of CO2 every year,
which represents <span id="ph_office_way_COP21"></span> % of COP21 target.
-->
</p>
</div>

<!-- <button class="collapsible">Équipement bureau</button>
<div class="content" id="user_devices_ph"> -->
<div id="user_devices_ph" class="tabcontent2">
</div>



<!-- <button class="collapsible">Alimentation</button>
<div class="content"> -->
<div id="user_meals" class="tabcontent2">
    <p>Régime alimentaire : <select name="input_meal_profile"></select>, objectif : <select name="input_new_meal_profile"></select><p>
    <p class="whatif">
      Cet objectif de changement de régime alimentaire conduit à une économie de <span class="value" name="ph_meal_gain"></span> kg.CO2 par an,
      soit l&#39;équivalent de <span class="value" name="ph_meal_COP21"></span> % de la cible COP21 pour les seuls repas pris au travail.
      En prenant en compte les 2x365 repas annuels, l&#39;économie représente <span class="value" name="ph_meal_full_gain"></span> kg.CO2 par an,
      soit <span class="value" name="ph_meal_full_COP21"></span> % de la cible COP21.
    </p>
  <div id="config_meal">
    <button class="collapsible">Détails des repartitions des repas sur une semaine</button> 
    <div class="content">
    <p></p>
    <table>
      <tr><th>type de repas</th><th>quantité</th><th></th></tr>
      <tr>
          <td>carné (boeuf/veau)</td>
          <td> <input type="number" step="0.5" min="0" max="14" name="input_user_beef"> </td>
          <td class="note">inclut 150g de viande</td></tr>
      <tr>
          <td>carné (porc/volaille)</td>
          <td> <input type="number" step="0.5" min="0" max="14" name="input_user_chicken"></td>
          <td class="note">inclut 150g de viande</td></tr>
      <tr>
          <td>poisson</td>
          <td><input type="number" step="0.5" min="0" max="14" name="input_user_fish"></td>
          <td class="note">inclut 150g de poisson</td></tr>
      <tr>
          <td>végé</td>
          <td><input type="number" step="0.5" min="0" max="14" name="input_user_vege"></td>
          <td class="note">inclut 2 oeufs</td></tr>
      <tr>
          <td>végan</td>
          <td><input type="number" step="0.5" min="0" max="14" name="input_user_vegan"></td>
          <td class="note"></td></tr>
      <tr class="highlight">
          <td>Total</td>
          <td><span name="ph_total_meals"></span></td>
          <td class="note">devrait être égal à 14, dans le cas contraire les données sont automatiquement normalisées</td></tr>
    </table>
    <p></p>
    </div>
  </div>
</div>



<!-- <button class="collapsible">Clusters de calculs</button>
<div class="content" id="user_clusters_ph"> -->
<div id="user_clusters_ph" class="tabcontent2">
  <!--<p></p>
  <table>
    <tr><td>Plafrim :</td>
        <td><input type="number" step="0.1" min="0" max="100" id="input_user_plafrim_percentage"> <span class="unit">%</span></td>
        <td class="note">pourcentage d&#39;utilisation de la plateforme par an</td></tr>

    <tr><td>Autres clusters :</td>
        <td><input type="number" step="10" min="0" max="999999" id="input_user_cluster_cpu_hours"> <span class="unit">heures</span></td>
        <td class="note">
          consommation totale d&#39;un CPU: <input type="number" step="10" min="0" max="9999" id="input_user_cluster_conso">W &nbsp;
          PUE du cluster: <input type="number" step="0.1" min="1.2" max="10" id="input_user_cluster_pue">
        </td></tr>
  </table>
  <p></p>-->
  <!--<p>TODO : ajouter le support pour les autres clusters de calculs, e.g., GRID5000, etc.</p>-->
</div>



<!-- <button class="collapsible">Notes</button>
<div class="content"> -->
<div id="user_notes" class="tabcontent2">
  <p>
    <!--<ul>
        <li>"électricité support" : obtenue à partir de la conso du bâtiment Inria-Bdx divisé par une estimation du nombre d&#39;agents dans le bâtiment.</li>
    </ul>-->
  </p>
  <div class="warning">
    <p>
      Attention, cela ne permet pas d&#39;évaluer le bilan carbone de ses recherches, il manque notamment:
      <ul>
        <li>la part du bilan des équipes supports (RH, SAF, assistante, etc)</li>
        <li>la part du bilan des agences de financements (ANR, europe, etc.)</li>
        <li>la part du bilan des supports de nos publications (conférence, revues, vidéos, etc.)</li>
        <!--<li>les missions non prises par Oreli</li>-->
        <li>etc.</li>
      </ul>
    </p>
  </div>
</div>
`;

// '

	var user_interface = {};

	user_interface.update = function() {

      g_user_data  = assemble_user_data(g_year);
      g_user_data2 = assemble_user_data(g_year,2);

      // update new officeway gains
      {
        var cur_CO2 = 0;
        for(var k in g_user_data.to_office) { cur_CO2 += g_user_data.to_office[k] * conv.to_CO2[k]; }
        var new_CO2 = 0;
        for(var k in g_user_data2.to_office) { new_CO2 += g_user_data2.to_office[k] * conv.to_CO2[k]; }
        gain_CO2 = cur_CO2 - new_CO2;
        document.getElementById('ph_office_way_gain').textContent =  toFixed(gain_CO2,2);
        document.getElementById('ph_office_way_COP21').textContent = toFixed(100*Math.abs(gain_CO2)/params.constants.COP21_CO2,2);
      }

      
      // yearly_user_PC.update(g_year,g_value_type);
      yearly_user_BP1.update(g_year,g_value_type);
      yearly_user_BP2.update(g_year,g_value_type);
      detail_user_PC.update();
      
      //update_local_flight( g_year);
      
      //trigger_onchange(document.getElementById('input_lifetime_extension'));

      // update hours per day
      // {
      //   power_CO2 = sum_power_consumption(g_data.user.devices) * conv.to_CO2.elec;
      //   current_CO2 = params.hours_per_day*g_data.user.working_days*power_CO2;
      //   new_CO2 = 24*365*power_CO2;
      //   lost_CO2 = new_CO2-current_CO2;
      //   document.getElementById('ph_hours_per_day_lost').textContent = toFixed(lost_CO2,1);
      //   document.getElementById('ph_hours_per_day_COP21').textContent = toFixed(100*Math.abs(lost_CO2)/params.constants.COP21_CO2,1);
      // }
    }

	// copy team's tarvels to user's travels by normalizing km by the number of travelers
    function assemble_user_data(selected_year,suffix) {
      user_name = "user";
      if(suffix)
        user_name += suffix;
      user_param = g_data[user_name];

      // FIXME we currently enforce equal working days
      user_param.working_days = g_data.user.working_days; 

      var num_meals = 0;
      for(var k in user_param.meals) { num_meals += user_param.meals[k]; }

      // automatically replace local flights by train if AUTO.
      var input2_flight_loc = document.getElementById('user_mission_table_input2_travels_flight_loc');
      var input2_train      = document.getElementById('user_mission_table_input2_travels_tgv');
      if(suffix==2  && input2_flight_loc && (input2_flight_loc.disabled)
                    && input2_train && (input2_train.disabled) )
      {
        input2_train.value      = g_data.user2.travels['tgv']      = g_data.user.travels['tgv'] + g_data.user.travels['flight_loc'];
        input2_flight_loc.value = g_data.user2.travels['flight_loc'] = 0;
      }

      // update gains
      if(suffix==2) {
        var flight_CO2 = g_data.user.travels['flight_loc'] * conv.to_CO2.flight_loc;
        var train_CO2 =  g_data.user.travels['tgv']        * conv.to_CO2.tgv;
        var gain_CO2 = flight_CO2-train_CO2;
        document.getElementById('ph_user_local_flight_gain').textContent = toFixed(gain_CO2,2);
        document.getElementById('ph_user_local_flight_COP21').textContent = toFixed(100*Math.abs(gain_CO2)/params.constants.COP21_CO2,2);
      }

      var elec_support   = params.labo.elec / params.labo.workers;

      var devices = process_raw_device_list(user_param.devices,{working_days:user_param.working_days});
      var elec_device = keyval_sumall(devices.elec);

      var res = {
        travels: clone(user_param.travels),
        to_office: clone(user_param.to_office,user_param.working_days),
        devices: devices.grey,
        meals: clone(user_param.meals,user_param.working_days/num_meals),
        energy: { // TODO remove ?
        	gaz_nat_m3: conv.to_m3.gaz_nat_kWh * params.labo.gaz_nat_kWh / params.labo.workers,
        	heatnet: params.labo.heatnet / params.labo.workers,
    			env_elec: elec_support,
    			device_elec:  elec_device,
    			// 'elec_plafrim':  elec_plafrim,
        },
        clusters: process_cluster_list(g_data['user'].clusters),
        // {
        //   'elec_plafrim':   elec_plafrim,
        //   'grey_CO2':       params.plafrim.grey_CO2*user_param.plafrim_percentage/100,
        //   'elec_clusters':  elec_clusters
        // },
        // environment: clone(yData.environment, 1/yData.meta.workers ),

          // env_elec: yData.environment.elec / yData.meta.workers, // FIXME
          // support:  yData.environment.support / yData.meta.workers, // reflect nat/local support (to_office + meals + grey + elec + ...)
        // },
        // home: user_param.home,
        // COP21: {COP21:params.constants.COP21_CO2},
        // french_citizen: params.default_french_citizen,
      };

      res.devices.device_elec = elec_device;
      // res.environment.env_elec = elec_total - elec_device;
      // res.home.meals_CO2 = sum_meals('CO2',user_param.meals) * (2*365-user_param.working_days)/user_param.working_days;

      return res;
    }

	var g_user_data  = assemble_user_data(g_year);
	var g_user_data2 = assemble_user_data(g_year,2);

	function make_user_data_for_barplot(selected_year, value_type, opt) {

		// remove redundancies :
		var user_data1_cpy = clone_obj(g_user_data);
		var user_data2_cpy = clone_obj(g_user_data2);

		user_data1_cpy.energy.device_elec = 0;
		user_data2_cpy.energy.device_elec = 0;
		user_data1_cpy.energy.elec_plafrim = 0;
		user_data2_cpy.energy.elec_plafrim = 0;

	    if(opt.stacked=='full') {

	      var full_data1 = compute_CO2_and_kWh(user_data1_cpy);
	      var full_data2 = compute_CO2_and_kWh(user_data2_cpy);

	      // stack data
	      var subgroups = ['travels','to_office','devices','meals','energy','clusters'];
	      subgroups.reduce_list = ['travels','to_office','devices','meals','energy','clusters'];
	      var yReduced1 = ungoup(full_data1, subgroups);
	      var r1 = process_group(yReduced1,'actuel')
	      var r2 = process_group(ungoup(full_data2, subgroups),'objectif');
	      r2.dim = 0.5;//params.viz.mosaic_barplot ? 1 : 0.5;
	      var res = [r1,r2];
	      return res;
	    } else {
	      var datatmp = compute_CO2_and_kWh(user_data1_cpy, {}, user_data2_cpy);
	      if(!(params.viz.user_detailed_barplot || params.viz.mosaic_barplot)) {
	        //console.log(datatmp);
	        var reduce_list = ['travels','to_office','devices','meals','energy','elec_group','clusters','environment'];
	        return partial_reduce_inplace(datatmp, reduce_list);
	      }
	      return datatmp;
	    }
	}

	var yearly_user_PC    = {};//make_yearly_pie_chart(g_year,g_value_type,'user');
	var detail_user_PC    = make_details_pie_chart(z => g_user_data,z => g_user_data2,'user');
	var yearly_user_BP1   = make_yearly_barplot([g_user_data,g_user_data2], g_year,g_value_type,'user',make_user_data_for_barplot,{dir:'vertical',width: 500},{click: function(d) {detail_user_PC.update(d.data.group_name);}});
	var opt = {
	  dir: 'vertical',
	  stacked: 'full',
	  width: 150,
	};
	var yearly_user_BP2   = make_yearly_barplot([g_user_data,g_user_data2], g_year,g_value_type,'user',make_user_data_for_barplot,opt);

	init_num_controller('input_user_working_days',g_data.user,'working_days', null, user_interface.update);
    // init_num_controller('input_user_plafrim_percentage',g_data.user,'plafrim_percentage', null, user_interface.update);
    // init_num_controller('input_user_plafrim_percentage',g_data.user2,'plafrim_percentage', null, user_interface.update);
    init_num_controller('input_user_bat_elec',params.labo,'elec', null, user_interface.update);
    init_num_controller('input_user_bat_gaz',params.labo,'gaz_nat_kWh', null, user_interface.update);
    init_num_controller('input_user_bat_heatnet',params.labo,'heatnet', null, user_interface.update);
    init_num_controller('input_user_heatnet_factor',conv.to_CO2,'heatnet', null, user_interface.update);
    init_num_controller('input_user_bat_workers',params.labo,'workers', null, user_interface.update);

    // init_num_controller('input_user_cluster_cpu_hours',g_data.user,'cluster_cpu_hours', null, user_interface.update);
    // init_num_controller('input_user_cluster_cpu_hours',g_data.user2,'cluster_cpu_hours', null, user_interface.update);

    // init_num_controller('input_user_cluster_conso',params.clusters,'watt_per_cpu', null, user_interface.update);
    // init_num_controller('input_user_cluster_pue',params.clusters,'pue', null, user_interface.update);

    // checkbox to split/unsplit the user-report bar plot
    document.getElementById('user_detailed_barplot').addEventListener("change", function() {
        params.viz.user_detailed_barplot = this.checked;
        user_interface.update();
    } );

    document.getElementById('mosaic_barplot').addEventListener("change", function() {
        params.viz.mosaic_barplot = this.checked;
        user_interface.update();
    } );

    // document.getElementById('fuse_piechart_comp').addEventListener("change", function() {
    //     params.viz.fuse_piechart_comp = this.checked;
    //     user_interface.update();
    // } );

    init_office_way('user_office_way_ph', g_data.user.to_office, g_data.user2.to_office, user_interface.update);
    init_devices('user_devices_ph', g_data.user, g_data.user2, user_interface.update);
    init_clusters('user_clusters_ph', g_data.user, user_interface.update);

    //------------------------------------------------------------
    // office equipments
    //------------------------------------------------------------
    {
      // grey_types.forEach(function(item) {
      //   number_el   = document.getElementById('input_user_'+item);
      //   lifetime_el = document.getElementById('input_lifetime_'+item);

      //   number_el  .value = g_data.user.devices[item];
      //   lifetime_el.value = params[item].duration;

      //   number_el.addEventListener("change", function() {
      //     g_data.user.devices[item] = this.value;
      //     user_interface.update();
      //   });

      //   lifetime_el.addEventListener("change", function() {
      //     params[item].duration = this.value;
      //     user_interface.update();
      //   });
      // });

      // document.getElementById('input_lifetime_extension').value = 2;
      // document.getElementById('input_lifetime_extension').addEventListener("change", function() {
      //   current_CO2 = 0;
      //   new_CO2 = 0;
      //   ext = this.value;
      //   grey_types.forEach(function(item) {
      //     current_CO2 = current_CO2 + g_data.user.devices[item]*conv.to_CO2[item]/ params[item].duration;
      //     new_CO2     = new_CO2     + g_data.user.devices[item]*conv.to_CO2[item]/(params[item].duration+ext);
      //   });
      //   gain_CO2 = current_CO2-new_CO2;
      //   document.getElementById('ph_lifetime_gain').textContent =  toFixed(gain_CO2,1);
      //   document.getElementById('ph_lifetime_COP21').textContent = toFixed(100*Math.abs(gain_CO2)/params.constants.COP21_CO2,1);
      // });

      // document.getElementById('ph_hours_per_day').textContent = params.hours_per_day;

    }

    add_user_pro_travels_rows('user_mission_table', user_interface.update);

    //------------------------------------------------------------
    // meals
    //------------------------------------------------------------

    var g_meal_profile1 = 'carné';
    var g_meal_profile2 = 'carné 1x / sem';
    function refresh_meal_profiles()
    {
      meal_profile1_els = document.getElementsByName('input_meal_profile');
      meal_profile2_els = document.getElementsByName('input_new_meal_profile');

      meal_profile1_els.forEach((el) => {el.value = g_meal_profile1;});
      meal_profile2_els.forEach((el) => {el.value = g_meal_profile2;});

      //console.log("g_meal_profile1:" + g_meal_profile1)
      g_data.user.meals   = meal_profiles[g_meal_profile1];
      g_data.user2.meals  = meal_profiles[g_meal_profile2];
      //console.log(meal_profiles);

      // refresh number of meals per type from stored parameters
      meal_types.forEach(function(item) {
        number_els = document.getElementsByName('input_user_'+item);
        number_els.forEach((el) => {el.value = g_data.user.meals[item]});
      });

      // update sum of meals
      nb_meals = 0;
      meal_types.forEach(function(item) { nb_meals += g_data.user.meals[item]; } );
      document.getElementsByName('ph_total_meals').forEach( (o) => {o.textContent = nb_meals});
      
      // refresh gains
      current_CO2 = sum_meals('CO2',g_data.user.meals);
      new_CO2 = sum_meals('CO2',g_data.user2.meals);

      gain_CO2 = current_CO2-new_CO2;
      document.getElementsByName('ph_meal_gain').forEach((el) => {el.textContent =  toFixed(gain_CO2,2)});
      document.getElementsByName('ph_meal_COP21').forEach((el) => {el.textContent = toFixed(100*Math.abs(gain_CO2)/params.constants.COP21_CO2,2)});

      gain_CO2 = gain_CO2/g_data.user.working_days*365*2;
      document.getElementsByName('ph_meal_full_gain').forEach((el) => {el.textContent =  toFixed(gain_CO2,2)});
      document.getElementsByName('ph_meal_full_COP21').forEach((el) => {el.textContent = toFixed(100*Math.abs(gain_CO2)/params.constants.COP21_CO2,2)});

      user_interface.update();
      if(perso_interface)
      	perso_interface.update();
    }

    user_interface.init_meal_profiles = function()
    {
      meal_profiles['perso'] = g_data.user.meals;

      meal_profile1_els = document.getElementsByName('input_meal_profile')
      meal_profile2_els = document.getElementsByName('input_new_meal_profile')
      for(var item in meal_profiles) {
        var opt_el = document.createElement('option');
        opt_el.value = item;
        opt_el.innerHTML = tr(item);
        meal_profile1_els.forEach((el) => {el.appendChild(opt_el.cloneNode('deep'))});
        meal_profile2_els.forEach((el) => {el.appendChild(opt_el.cloneNode('deep'))});
      }
      meal_profile1_els.forEach((el) => {el.addEventListener("change", function() {
        g_meal_profile1 = this.value;
        refresh_meal_profiles();
      })});

      meal_profile2_els.forEach((el) => {el.addEventListener("change", function() {
        g_meal_profile2 = this.value;
        refresh_meal_profiles();
      })});

      meal_types.forEach(function(item) {
        number_els = document.getElementsByName('input_user_'+item);
        number_els.forEach((el) => {el.addEventListener("change", function() {
          g_data.user.meals[item] = Number(this.value);
          refresh_meal_profiles();
        })});
      });

      refresh_meal_profiles();
    }

    user_interface.update();
    detail_user_PC.update('travels');

    return user_interface;
}

