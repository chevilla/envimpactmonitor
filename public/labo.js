


function add_labo_pro_travels_rows(table_id,update_callback) {
  return add_rows(document.getElementById(table_id),z => center_default[g_year],undefined,update_callback,
    [
      {group:'travels',     id:'flight_loc',  min:0, max:9999999, step:100, unit:'km', label:'Vols "locaux" :', note:'(destinations atteignables en train de manière raisonable)'},
      {group:'travels',     id:'flight',      min:0, max:9999999, step:1090, unit:'km', label:'Autres vols :', note:''},
      {group:'travels',     id:'tgv',         min:0, max:9999999, step:1090, unit:'km', label:'Train :', note:'(tgv)'},
      // {group:'travels',     id:'bus',         min:0, max:9999999, step:1000, unit:'km', label:'Bus :', note:''},
      // {group:'travels',     id:'car',         min:0, max:9999999, step:1000, unit:'km', label:'Voiture :', note:''},
    ]);
}

function init_labo_interface() {
	function update_myself() {
      allY_labo_BP.update(g_value_type);
      g_center_data = assemble_center_data(g_year);
      yearly_labo_BP.update(g_year,g_value_type);
    }

	function assemble_center_data(selected_year) {

      var kg2T = 0.001;

      var normalize = document.getElementById("input_lab_normalize").checked;
      if(normalize)
        kg2T = 1;

      var num_meals = 0;
      for(var k in center_default.cafeteria) { num_meals += center_default.cafeteria[k]; }

      // update gains
      var flight_CO2 = center_default[selected_year].travels['flight_loc'] * conv.to_CO2.flight_loc;
      var train_CO2 =  center_default[selected_year].travels['flight_loc'] * conv.to_CO2.tgv;
      var gain_CO2 = (flight_CO2-train_CO2)*kg2T;
      document.getElementById('ph_labo_local_flight_gain').textContent = toFixed(gain_CO2,1);

      var elec_total   = center_default.elec[selected_year];
      var elec_device  = params.hours_per_day * params.working_days * sum_power_consumption(center_default.devices);
      var elec_plafrim = params.plafrim.elec*params.plafrim.pue;

      var res = {
        travels: clone(center_default[selected_year].travels, (normalize ? 1/center_default.travelers : 1) * kg2T),
        
        to_office: clone(center_default.to_office,params.working_days*(normalize ? 1 : center_default.workers) * kg2T),

        devices: clone(center_default.devices, (normalize ? 1/center_default.building_workers : 1)*kg2T),

        cafeteria: clone(center_default.cafeteria,params.working_days/num_meals*(normalize ? 1 : center_default.workers) * kg2T),

        elec_group: {
          'env_elec': (elec_total-elec_device-elec_plafrim) * (normalize ? 1/center_default.building_workers : 1)*kg2T,
          'elec_plafrim': elec_plafrim * (normalize ? 1/center_default.researchers : 1)*kg2T,
          'device_elec': elec_device * (normalize ? 1/center_default.building_workers : 1)*kg2T,
        },

        plafrim: {
          'elec_plafrim_nodes': params.plafrim.elec*(normalize ? 1/center_default.researchers : 1)*kg2T,
          'elec_plafrim_clim': params.plafrim.elec*(params.plafrim.pue-1)*(normalize ? 1/center_default.researchers : 1)*kg2T,
          'grey_CO2': params.plafrim.grey_CO2*(normalize ? 1/center_default.researchers : 1)*kg2T
        },
      };

      // scale by default lifetime
      for(var k in res.devices) {
        res.devices[k] /= devices[k].duration;
      }

      res.devices.device_elec = elec_device * (normalize ? 1/center_default.building_workers : 1)*kg2T;

      return res;
    }


	var g_center_data = assemble_center_data(g_year);

  var allY_labo_BP = make_all_years_barplot(center_default, g_value_type,'labo',
      {click: function(d) {
          g_year = d.year;
          yearly_labo_BP.update(g_year,g_value_type);
          //update_local_flight(g_year);
          labo_mission_table.update();
          update_myself();
      }});

	var yearly_labo_PC    = {};//make_yearly_pie_chart(g_year,g_value_type,'labo');
	var yearly_labo_BP    = make_yearly_barplot(null, g_year,g_value_type,'labo', (y,v) => compute_CO2_and_kWh(g_center_data), {dir:'horizontal', width: 800});

	init_num_controller('labo_nb_office_users',center_default,'building_workers', null, update_myself);
	init_num_controller('labo_nb_workers',center_default,'workers', null, update_myself);
	init_num_controller('labo_nb_travelers',center_default,'travelers', null, update_myself);

	init_num_controller('labo_nb_pc_in_use',center_default.devices,'desktop', null, update_myself);
	init_num_controller('labo_nb_laptop_in_use',center_default.devices,'laptop', null, update_myself);
	init_num_controller('labo_nb_screen_in_use',center_default.devices,'screen', null, update_myself);

	// checkbox to normalize lab-report by lab size
  document.getElementById('input_lab_normalize').addEventListener("change", function() {
      // params.viz.normalize_barplot = this.checked;
      update_myself();
  } );

  var labo_mission_table = add_labo_pro_travels_rows('labo_mission_table', update_myself);

  update_myself();
}
